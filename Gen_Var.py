import sys,os,time

PDB = sys.argv[1].split('.')[0]
Chain = sys.argv[2]
Mut = sys.argv[3]

t = str(int(time.time() * 10000))

os.system('mkdir VAR_' + t + '_' + PDB + '_' + Chain)

os.system('cp Scripts/* ' + 'VAR_' + t + '_' + PDB + '_' + Chain)
os.system('cp ' + sys.argv[1] + '.pdb VAR_' + t + '_' + PDB + '_' + Chain)

os.chdir('VAR_' + t + '_' + PDB + '_' + Chain)

os.system('python MakeVariant.py ' + PDB + ' ' + Chain + ' ' + Mut)



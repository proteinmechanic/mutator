# Mutator

### Want to use Mutator on your own machine?

Skip to the last section of this document to find instructions on how to do this. 

### Method:

  - VMD is used for both pre and post processing of structures.
  - Modeller is used to generate variations of protein structures. 
  - NAMD is used for energy minimization making use of CHARMM36FF.
  - NGL is used for protein structure visualization. 
  - A mapping between uniprot and RCSB PDB structures is obtained from the SIFTS record.
  - Protein BLAST is used to compare protein sequences for generating supplementary data.
  - Python3.8 and bash are used for passing data between programs.

### User Interface:

#### Generating a variant

    http://mutator.bii.a-star.edu.sg/ver3/

This page provide a user interface which can be used to provide input data to Mutator. This includes providing 

- A 4-letter PDB ID
- A maximum 2-letter PDB chain ID
- Variation that has to be introduced into the protein structure. This must follow the format where the reference amino acid, position and alternate amino acid are provided as a contiguous string, e.g., VAL20GLY. 
- The reference amino acid, VAL in the example used above must be present at position 20 in the protein structure.

A successfull variant generation will load a result page which will show:

- Both the wild-type and variant structures post-minimization. The residues at positions on which variants have been created will be shown in ball+stick mode - whereas the remainder of the structure will be in cartoon representation. Non-protein molecules will not be shown here.  
- A box briefly listing some information regarding the change in potential energy and RMSD, along with calculated values. 
- A residue-based RMSD plot to show which regions have moved in the minimization step to accomodate any changes, along with arrows marking positions on changes have been introduced. 
- A table listing values in the following column:

        > 1. Pos (Position) : Position on which the variation is created.
        > 2. Ref (Reference): The reference amino acid present at the position in "1" above.
        > 3. Alt (Alternate): The alternate amino acid to be generated at the position in "1" above.
        > 4. dVol (Change in side chain volume): Calculated as Av - Rv. Where Av is the side chain volume of the alternate amino acid and Rv is the side chain volume of the reference amino acid
        > 5. dH (Change in hydropathy): Calculated as Ah - Rh. Where Ah is the hydropathy index of the alternate amino acid and Rv is the  hydropathy index of the reference amino acid
        > 6. Ref_E (Electrostatic term for wild-type): Electrostatic interaction between reference amino acid and the other amino acids in the wild-type structure.  
        > 7. Ref_V (vdW term for wild-type): vdW interaction between reference amino acid and the other amino acids in the wild-type structure.  
        > 8. Ref_NB (non-bonded term for wild-type): Sum of non-bonded terms (Elec [9] and vdW [10])for the wild-type structure 
		> 9. Alt_E (Electrostatic term for variant structure): Electrostatic interaction between the alternate amino acid and the other amino acids in the variant structure.  
        > 10. Alt_V (vdW term for variant structure): vdW interaction between the alternate amino acid and the other amino acids in the variant structure. 
        > 11. Alt_NB (non-bonded term for variant structure): Sum of non-bonded terms (Elec [9] and vdW [10])for the variant structure 
        > 12. dProx (Change in proximal energy): Difference between 11 (Alt_NB) and 8 (Ref_NB)

- In case multiple variations are created the table will have multiple rows, one for each variation created.

From the landing page     

    http://mutator.bii.a-star.edu.sg/ver3/

three other interfaces are also accessible. Namely:

- Ability to search for existing variants 
- Ability to search for existing structures (including predictions by AlphaFold) in which variations are to be introduced
- Uploading protein structures (not available in RCSB PDB or predicted by AlphaFold) in which variations are to be introduced

Each of these is decribed below. 

#### Searching for existing variants
	
	http://mutator.bii.a-star.edu.sg/ver3/index2.html

Each protein structure is mapped to its respective Uniprot ID. To search for variants, a valid Uniprot ID is needed. 

A successfull search for variants will return a table with interactive rows where each row will have the following columns: 


        > 1. Uniprot ID : The query Uniprot ID
        > 2. Length : The number of amino acids in the protein sequence, where the sequence was obtained from Uniprot.
        > 3. PDB_Chain : The PDB_Chain ID of the protein mapping against the Uniprot ID, as indicated by the SIFTS record. 
        > 4. PDB_Length : The length of the protein sequence as provided by RCSB PDB against the PDB_Chain in 3 above. 
        > 5. % Similarity : Similarity reported by protein blast when comparing the Uniprot sequence with the sequence from RCSB PDB. 
        > 6. % Identity : Identity reported by protein blast when comparing the Uniprot sequence with the sequence from RCSB PDB.
        > 7. Align  Length : Length of the alignment reported by protein blast when comparing the Uniprot sequence with the sequence from RCSB PDB.
        > 8. Distance : Number of mismatches between the Uniprot sequence and the sequence from RCSB PDB.
		> 9. Variation : A comma delimited string of mismatches between the Uniprot sequence and the sequence from RCSB PDB. Numbering in this case is conserved from BLAST and might not match the numbering of the protein structure.
		
Each row in the table is interactive and links to additional data in RCSB PDB. To map sequence numbers to numbers in the protein structure, clicking the row will redirect the user to a feature viewer for the protein chain of choice in RCSB PDB from where the sequence to structure position mapping can be recovered.
		
#### Searching for existing structures (including predictions by AlphaFold) in which variations are to be introduced 
	
	http://mutator.bii.a-star.edu.sg/ver2/index3.html

This interface connects to a database which comprises data coallated from AlphaFold, Uniprot and RCSB PDB. Keywords provided by the user can be used to query this database and pull out protein structures.   		
This search function helps identify structures if the PDB ID is not known or if predictions of AlphaFold are to be used. An interactive table is populated which lists all matches against the user defined query. 
By choosing a row in the table, a new page is loaded which provides access to a form (only the mutation can be provided in this form. The structure name and chain ID are fixed. To change these, go to the previous page and choose a different entry or change the URL accordingly).
Next to the form a display window loads a structure (loading time depends on users internet speed), allowing users the ability to mouse-over and analyse different parts of the structure and identify residues to vary.
In this display, all structures are coloured by bfactor. In case of predictions from AlphaFold the colour will provide a measure of the confidence score (pLDDT). In case of experimentally determined structures, all residues will have the same colour. 
Once the mutation is provided and variant structures are requested, the following page will comprise the same interface as explained in the Generating a variant section above in case of RCSB PDB structures.
If structure selected is a prediction by AlphaFold, steps comprising minimization, energetic evaluation and quantifying structural changes are skipped and these entries will be blank. 

The purpose of skipping steps for AlphaFold predictions is because these structures can in certain cases comprise numerous regions which have poor confidence scores (pLDDT), e.g., see the prediction Zinc finger protein 532 (AF-A0A0G2JZQ5-F1-model_v1; http://mutator.bii.a-star.edu.sg/ver3/gv.php?SID=AF-A0A0G2JZQ5-F1-model_v1). 
Minimizing structures like this and any energetics derived will be prone to error and therefore skipped. 

The user is welcome to download the structure, change its name to remove special characters and reupload the structure using the interface explained below. 

In case AlphaFold predictions are used with this interface the final results page will have only the wildtype structure displayed with the table listing pLDDT of the variant amino acid position - which is also colour coded to match the legend provided next to the display. The structure is coloured by bfactor but the categories are binned to match the AlphaFold EBI pattern.


#### Uploading protein structures (not available in RCSB PDB or predicted by AlphaFold) in which variations are to be introduced

	http://mutator.bii.a-star.edu.sg/ver3/index4.html

This interface is similar to the Generate a variant interface explained above, with a difference that the user can upload a structure. 
This interface is useful in cases where the structure is not available in RCSB PDB or the user intends to use a structure which has been modified from its original state as provided by RCSB PDB. 

### To run Mutator locally (Mac & Linux only):

  - Download and set up VMD
  - Download and set up NAMD
  - Obtain CHARMM36 forefield files
  - Download and set up Modeller (scripts have been tested with the Modeller build provided for Anaconda). During this process you will need to register for a licence key (free). Obtain the licence key and set it up accordingly.
  - Clone this repository
  - In the python script Scripts/MakeVariant.py - in lines 6 and 7 pass the NAMD and VMD paths.
  - In the file Scripts/BP_Energy.Template (line 16) - provide the NAMD path following the -exe flag.
  - The scripts were built and tested on mac using GNU grep (ggrep). For running on Linux machines change "ggrep" in lines 42, 115 and 119 to just "grep".
  - Two sample structures are provided in the inluded folder. Move each of the structures to the same location as the script Gen_Var.py. The following commands should generate variants successfully.
     * python Gen_Var.py AF-A0A286Y901-F1-model_v1 A GLY339ARG,PRO341GLY
     * python Gen_Var.py 1hv4 A ARG31GLU 
     * Gen_Var.py must be followed by the PDB file name (without file extension), chain in which to generate the variation and a comma separated list of variants (all caps). 
     * The first command will generate one final structure (MT.pdb) which will have two positions modified (339, 341).
     * The second command will generate one final structure (MT.pdb) which will have one position modified (31).
     * A new folder VAR_"TIMESTAMP"\_PDBFILENAME_CHAINNAME will be created. This will hold the WT/MT structures, along with a file Stats_Table.dat listing the metrics. The file DiffE and RMSD.DIFF will hold single values indicated the change in potential energy (MT - WT) and RMSD difference (MT compared to WT).
     * A PNG file will also be created MT.png which will plot the RMSD per position using the data held in the file RMSD.dat


##### Spot a bug:

Get in touch by emailing

Ashar Malik (asharjm@bii.a-star.edu.sg, asharjm@gmail.com)
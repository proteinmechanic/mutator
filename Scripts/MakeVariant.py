import numpy as np
import os,glob,sys,subprocess
import matplotlib.pyplot as pl
import matplotlib.patches as mpatches

NAMD = '/Users/proteinmechanic/local/software/NAMD/namd2'
VMD = '/Users/proteinmechanic/local/software/vmd.app/Contents/MacOS/startup.command'

def t2o(aa):
	ol = np.asarray("A C D E F G H I K L M N P Q R S T V W Y".split())
	tl = np.asarray("ALA CYS ASP GLU PHE GLY HIS ILE LYS LEU MET ASN PRO GLN ARG SER THR VAL TRP TYR".split())
	return ol[np.where(aa == tl)[0]][0]


PDB = sys.argv[1]
Chain = sys.argv[2]
Mut = sys.argv[3]

##########

os.system(VMD + ' -dispdev text -eofexit -args < extractChain2.tcl ' + PDB + ' ' + Chain + ' > VMD.LOG')

if os.path.isfile(PDB + '_' + Chain + '.pdb') != True:
	raise TypeError('Query PDB structure does not have the specified chain ID. Try with a different Chain ID.')
##########


_AA_Tab_ = np.genfromtxt('AA.table',dtype=str,delimiter='\t')
_AA_ =  np.genfromtxt('AA.table',dtype=str,delimiter='\t',usecols=1)



var = []

err = []

_pos_ = []

_STATS_ = []

# Get all ATOM data for bfac extraction
bfac_dat = subprocess.Popen('ggrep -P "^ATOM" ' + sys.argv[1] + '.pdb',stdout = subprocess.PIPE, shell = True).stdout.read().decode()
bfac_dat = bfac_dat.split('\n')[0:-1]

Rna, Rnu, bfac = [],[],[]

for i in bfac_dat:
	Rna.append(i[17:20].strip()) # Residue name	
	Rnu.append(i[22:26].strip()) # Residue number	
	bfac.append(i[60:66].strip()) 

Rna, Rnu, bfac = np.asarray(Rna), np.asarray(Rnu), np.asarray(bfac) 

for i in Mut.split(','):
	# Match i to residue list
	aa = i[0:3]
	pos = i[3:len(i)-3]
	aao = t2o(aa)
	aaF = t2o(i[len(i)-3:])
	############# Diff
	idxO = np.where(aao == _AA_)[0][0]
	idxF = np.where(aaF == _AA_)[0][0]
	dV = str(round(float(_AA_Tab_[idxF][3]) - float(_AA_Tab_[idxO][3]),2))
	dH = str(round(float(_AA_Tab_[idxF][4]) - float(_AA_Tab_[idxO][4]),2))

	try:
		idxLK = np.where(pos == Rnu)[0][0]
	except:
		idxLK = -999
	if idxLK != -999:
		if Rna[idxLK] == aa:
			var.append(i)
			_pos_.append(i[3:len(i)-3])
			if sys.argv[1][0:3] != "AF-":
				_STATS_.append([pos,aao,aaF,dV,dH])
			else:
				_STATS_.append([pos,bfac[idxLK],aao,aaF,dV,dH])
		else:
			err.append(i)
	else:
		err.append(i)

if len(err) > 0:
        raise ValueError('Inconsistency found on the following residue/s ' + ','.join(err) + ' :: Ensure that residue number/s and REF amino acid label/s are matching those in the structure (' + PDB + '_' + Chain + ')')

os.system('python mutate_model.py ' + sys.argv[1] + ' ' + Chain + ' '  + ",".join(var) + ' > m.log')

os.system(VMD + ' -dispdev text -eofexit -args < process_Distance.tcl ' + PDB + '_' + Chain + '.pdb' +  ' > /dev/null 2>&1')

############# Energetics
ener = []

confWT = ""
with open('conf.template') as f:
	for line in f:
		confWT += line

confWT = confWT.replace('_PDB_',PDB + '_' + Chain + '_psfgen.pdb').replace('_PSF_',PDB + '_' + Chain + '_psfgen.psf').replace('_OUT_','WT.dat')
	
with open('WT.conf','a') as f:
	f.write(confWT)

confMT = ""
with open('conf.template') as f:
	for line in f:
	        confMT += line

confMT = confMT.replace('_PDB_',PDB + '_' + Chain + '_Mutated_psfgen.pdb').replace('_PSF_',PDB + '_' + Chain + '_Mutated_psfgen.psf').replace('_OUT_','MT.dat')

with open('MT.conf','a') as f:
	f.write(confMT)


val = ""
val = subprocess.Popen(NAMD + ' WT.conf' + ' | ggrep -P "^ENERGY" | tail -n -1 | awk -F " " ' + "'{print $15}'",shell=True,stdout=subprocess.PIPE).stdout.read().decode()
ener.append(float(val.split('\n')[0]))

val = ""
val = subprocess.Popen(NAMD + ' MT.conf' + ' | ggrep -P "^ENERGY" | tail -n -1 | awk -F " " ' + "'{print $15}'",shell=True,stdout=subprocess.PIPE).stdout.read().decode()
ener.append(float(val.split('\n')[0]))

diffE = str(round(ener[1] - ener[0],3)) + ' kcal/mol'

with open('DifE.log','a') as f:
	f.write(diffE)

# Binding Pocket energetics

BP_WT = ""
with open("BP_Energy.Template") as f:
	for line in f:
		BP_WT += line

BP_WT = BP_WT.replace("_PSF_",PDB+ '_' + Chain + '_psfgen.psf').replace("_COOR_","WT.dat.coor").replace("_POS_", '"' + ",".join(_pos_) + '"').replace("_STATUS_","WT")

BP_MT = ""
with open("BP_Energy.Template") as f:
	for line in f:
		BP_MT += line

BP_MT = BP_MT.replace("_PSF_",PDB + '_' + Chain + '_Mutated_psfgen.psf').replace("_COOR_","MT.dat.coor").replace("_POS_",'"' + ",".join(_pos_) + '"').replace("_STATUS_","MT")

with open('BP_Energy.tcl','a') as f:
	f.write(BP_WT)
	f.write('\n')
	f.write(BP_MT)

os.system(VMD + ' -dispdev text -eofexit -args < BP_Energy.tcl > /dev/null 2>&1')

#####################################################


c_ = 0
with open('Stats_Table.dat','a') as _g_:
	if sys.argv[1][0:3] != "AF-":
		_g_.write('Pos\tWT\tMT\tdV\tdH\tWT_Elec\tWT_vdW\tWT_NB\tMT_Elec\tMT_vdW\tMT_NB\tDiff (MT-WT)\n')
	else:
		_g_.write('Pos\tConf(AF)\tWT\tMT\tdV\tdH\tWT_Elec\tWT_vdW\tWT_NB\tMT_Elec\tMT_vdW\tMT_NB\tDiff (MT-WT)\n')
	for i in _STATS_:
		pos = i[0]
		fn = 'Ener_WT_' + pos + '.log'
		with open(fn) as g:
			for line in g:
				pass
		lwt = line.split()[2:5]
		fn = 'Ener_MT_' + pos + '.log'
		with open(fn) as g:
			for line1 in g:
				pass
		lmt = line1.split()[2:5]
		c_ += 1
		str_ = ""
		if sys.argv[1][0:3] != "AF-":
			_g_.write(i[0] + '\t' + i[1] + '\t' + i[2] + '\t' + i[3] + '\t' + i[4] + '\t' + lwt[0] + '\t' + lwt[1] + '\t' + lwt[2] + '\t' + lmt[0] + '\t' + lmt[1] + '\t' + lmt[2] + '\t' + str(round(float(lmt[-1]) - float(lwt[-1]),3)) + '\n')
		else:
			_g_.write(i[0] + '\t' + i[1] + '\t' + i[2] + '\t' + i[3] + '\t' + i[4] + '\t' + i[5] + '\t' + lwt[0] + '\t' + lwt[1] + '\t' + lwt[2] + '\t' + lmt[0] + '\t' + lmt[1] + '\t' + lmt[2] + '\t' + str(round(float(lmt[-1]) - float(lwt[-1]),3)) + '\n')

#####################################################

pdb = PDB + '_' + Chain + '.pdb ' 
res_list = _pos_

sid = pdb.split('.')[0].split('_')[0] + '_' + pdb.split('.')[0].split('_')[1]

os.system(VMD + ' -dispdev text -eofexit < RMSD.tcl > /dev/null 2>&1')

if os.path.isfile('RMSD.dat') == True:
	d_0 = np.genfromtxt('RMSD.dat',dtype=float,delimiter='\t',usecols=0)
	d_1 = np.genfromtxt('RMSD.dat',dtype=float,delimiter='\t',usecols=1)

	fig, ax = pl.subplots(1,1)
	fig.set_figheight(6)
	fig.set_figwidth(8)

	if np.max(d_1) > 0.5:
		hg = 0.1 * np.max(d_1)
		pl.axis([np.min(d_0),np.max(d_0) + 1,-0.01,np.max(d_1)+0.1])
	else:
		hg = 0.05
		pl.axis([np.min(d_0),np.max(d_0) + 1,-0.01,0.5])

	_ax_ = ax.set_xlabel('Residue Number',fontsize=18)#.set_position(0.5, 0.5)
	ax.xaxis.set_label_coords(0.5, -0.165)
	pl.ylabel('Deviation ($\AA$)',fontsize=18)
	ax.tick_params(axis='both', which='major', labelsize=18)
	ax.tick_params(axis='both', which='minor', labelsize=18)
	ax.plot(d_0,d_1,'o')
	for i in res_list:
		str_ = 'arrow0 = mpatches.Arrow(' + i + ', ' + str(-1 * hg - 0.04)  + ', 0, ' + str(0.04)  + ', width = 5, color="red", clip_on=False)'
		exec(str_)
		ax.add_patch(arrow0)
	pl.tight_layout()
	pl.savefig('MT.png')



#####################################################



print('Variant structure successfully generated.')



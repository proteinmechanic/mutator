mol load pdb WT.pdb
mol load pdb MT.pdb

set sel0 [atomselect 0 "all and name CA"]
set sel1 [atomselect 1 "all and name CA"]

set RMSD [measure rmsd $sel0 $sel1]
puts $RMSD
set mx [measure fit $sel0 $sel1]
$sel0 move $mx
set RMSD [measure rmsd $sel0 $sel1]
set f [open RMSD.DIFF a]
puts $f $RMSD
close $f

set l0 [$sel0 get {x y z}]
set l1 [$sel1 get {x y z}]
set l2 [$sel1 get resid]
set l3 [$sel0 get resname]
set l4 [$sel1 get resname]

set lln [llength $l0]

set f [open RMSD.dat a]
for { set i 0 } { $i < $lln } { incr i } {
	puts $f [lindex $l2 $i]\t[veclength [vecsub [lindex $l0 $i] [lindex $l1 $i]]]
}
close $f



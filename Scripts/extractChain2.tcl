set PDB [lindex $argv 0] 
set ch [lindex $argv 1]
mol load pdb $PDB.pdb
set p _

set chxSEL [lsort -dict -unique [[atomselect top all] get chain]]

if {[lsearch -exact $chxSEL $ch] >= 0 & $ch ne "x" & $ch ne "y" & $ch ne "z"} {
        set sel1 [atomselect top "protein and chain $ch"]
        $sel1 writepdb $PDB$p$ch.pdb
        $sel1 delete
        set pdb $PDB$p$ch.pdb
        set id [lindex [split $pdb .] 0]
        set und _
        set id2 psfgen

        package require psfgen
        topology top_all36_prot.rtf

	pdbalias residue NVA VAL
	pdbalias residue AGQ TYR
	pdbalias residue MTY TYR
	pdbalias residue PTR TYR
	pdbalias residue TPQ TYR
	pdbalias residue TYI TYR
	pdbalias residue TYO TYR
	pdbalias residue TYS TYR
	pdbalias residue YOF TYR
	pdbalias residue FTR TRP
	pdbalias residue KYN TRP
	pdbalias residue TCR TRP
	pdbalias residue TPO THR
	pdbalias residue BXT SER
	pdbalias residue FGP SER
	pdbalias residue HSL SER
	pdbalias residue SAC SER
	pdbalias residue SEB SER
	pdbalias residue SEP SER
	pdbalias residue SET SER
	pdbalias residue SE7 SEC
	pdbalias residue HYP PRO
	pdbalias residue AME MET
	pdbalias residue CXM MET
	pdbalias residue FME MET
	pdbalias residue MHO MET
	pdbalias residue MSE MET
	pdbalias residue SME MET
	pdbalias residue ALY LYS
	pdbalias residue KCX LYS
	pdbalias residue KPI LYS
	pdbalias residue LLP LYS
	pdbalias residue M3L LYS
	pdbalias residue MLY LYS
	pdbalias residue MLZ LYS
	pdbalias residue LED LEU
	pdbalias residue NLE LEU
	pdbalias residue HIC HSE
	pdbalias residue NEP HSE
	pdbalias residue PVH HSE
	pdbalias residue FGL GLY
	pdbalias residue CGU GLU
	pdbalias residue MEQ GLN
	pdbalias residue PCA GLN
	pdbalias residue 6V1 CYS
	pdbalias residue BCS CYS
	pdbalias residue BPE CYS
	pdbalias residue CAF CYS
	pdbalias residue CAS CYS
	pdbalias residue CCS CYS
	pdbalias residue CME CYS
	pdbalias residue CSD CYS
	pdbalias residue CSO CYS
	pdbalias residue CSS CYS
	pdbalias residue CSU CYS
	pdbalias residue CSX CYS
	pdbalias residue OCS CYS
	pdbalias residue P1L CYS
	pdbalias residue R1A CYS
	pdbalias residue S2C CYS
	pdbalias residue SAH CYS
	pdbalias residue SCH CYS
	pdbalias residue SCS CYS
	pdbalias residue SCY CYS
	pdbalias residue SMC CYS
	pdbalias residue SNC CYS
	pdbalias residue YCM CYS
	pdbalias residue BFD ASP
	pdbalias residue BHD ASP
	pdbalias residue PHD ASP
	pdbalias residue AHB ASN
	pdbalias residue 2MR ARG
	pdbalias residue DA2 ARG
	pdbalias residue POK ARG
	pdbalias residue 1AC ALA
	pdbalias residue AIB ALA
	pdbalias residue ALS ALA
	pdbalias residue AYA ALA
	pdbalias residue DDZ ALA
	pdbalias residue ORN ALA
	pdbalias residue TIH ALA
	pdbalias residue HIS HSE

	pdbalias atom ILE CD1 CD

	segment $ch {pdb $pdb}
        coordpdb $pdb $ch
        guesscoord

        writepdb $id$und$id2.pdb
        writepsf $id$und$id2.psf

	cp $id$und$id2.pdb $PDB$p$ch.pdb
        ########################

        set sl_ "A C D E F G H I K L M N P Q R S T V W Y"
        set tl_ "ALA CYS ASP GLU PHE GLY HSE ILE LYS LEU MET ASN PRO GLN ARG SER THR VAL TRP TYR"

        mol delete all
        mol load pdb $id$und$id2.pdb
        set sel1 [atomselect top "all"]
        set l_sl_ []
        set seqid [lsort -dict -unique [$sel1 get {resid resname}]]
        for {set j1 0} {$j1 < [llength $seqid]} {incr j1} {
                set to_conv [lindex [lindex $seqid $j1] 1]
                set found_at [lsearch $tl_ $to_conv]
                set conv_to [lindex $sl_ $found_at]
                lappend l_sl_ $conv_to
        }
        $sel1 delete
        set fil [open $PDB$p$ch.VMDRid a]
        set NEWl_sl_ []
        for {set i 0} {$i < [llength $seqid]} {incr i} {
                if {$i eq 0} {
                        puts $fil $PDB$p$ch\t[lindex [lindex $seqid $i] 0]\t[lindex $l_sl_ $i]  
                        lappend NEWl_sl_ [lindex $l_sl_ $i]     
                } else {
                        set diff [expr [lindex [lindex $seqid $i] 0]- [lindex [lindex $seqid [expr $i-1]] 0]]
                        if {$diff > 1} {
                                for {set j 0} {$j < [expr $diff - 1]} {incr j} {
                                        puts $fil $PDB$p$ch\t[expr [lindex [lindex $seqid [expr $i - 1]] 0] + $j + 1]\t-
                                        lappend NEWl_sl_ -      
                                }
                                puts $fil $PDB$p$ch\t[lindex [lindex $seqid $i] 0]\t[lindex $l_sl_ $i]
                                lappend NEWl_sl_ [lindex $l_sl_ $i]
                        } else {
                                puts $fil $PDB$p$ch\t[lindex [lindex $seqid $i] 0]\t[lindex $l_sl_ $i]
                                lappend NEWl_sl_ [lindex $l_sl_ $i]
                        }
                }
        }
        close $fil
        set fil [open $PDB$p$ch.VMDsq a]
        puts $fil >$PDB$p$ch.VMD
        puts $fil [join $NEWl_sl_ ""]
        close $fil
}

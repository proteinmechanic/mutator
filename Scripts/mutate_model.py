#!/home/admin/local/miniconda3/bin/python

import sys,os
from modeller import *
from modeller.automodel import autosched
from modeller.optimizers import molecular_dynamics, conjugate_gradients
from modeller.automodel import autosched
import random
#
#  mutate_model.py
#
#     Usage:   python mutate_model.py modelname respos resname chain > logfile
#
#     Example: python mutate_model.py 1t29 1699 LEU A > 1t29.log
#
#
#  Creates a single in silico point mutation to sidechain type and at residue position
#  input by the user, in the structure whose file is modelname.pdb
#  The conformation of the mutant sidechain is optimized by conjugate gradient and
#  refined using some MD.
#
#  Note: if the model has no chain identifier, specify "" for the chain argument.
#

def make_restraints(mdl1, aln):
	rsr = mdl1.restraints
	rsr.clear()
	s = selection(mdl1)
	for typ in ('stereo', 'phi-psi_binormal'):
		rsr.make(s, restraint_type=typ, aln=aln, spline_on_site=True)
	for typ in ('omega', 'chi1', 'chi2', 'chi3', 'chi4'):
		rsr.make(s, restraint_type=typ+'_dihedral', spline_range=4.0,spline_dx=0.3, spline_min_points = 5, aln=aln,spline_on_site=True)


def optimize(atmsel, sched):
    #conjugate gradient
    for step in sched:
        step.optimize(atmsel, max_iterations=200, min_atom_shift=0.001)
    #md
    refine(atmsel)
    cg = conjugate_gradients()
    cg.optimize(atmsel, max_iterations=200, min_atom_shift=0.001)


#molecular dynamics
def refine(atmsel):
    # at T=1000, max_atom_shift for 4fs is cca 0.15 A.
    md = molecular_dynamics(cap_atom_shift=0.39, md_time_step=4.0,
                            md_return='FINAL')
    init_vel = True
    for (its, equil, temps) in ((200, 20, (150.0, 250.0, 400.0, 700.0, 1000.0)),
                                (200, 600,
                                 (1000.0, 800.0, 600.0, 500.0, 400.0, 300.0))):
        for temp in temps:
            md.optimize(atmsel, init_velocities=init_vel, temperature=temp,
                         max_iterations=its, equilibrate=equil)
            init_vel = False

PDB = sys.argv[1]
Chain = sys.argv[2]
Var = sys.argv[3]


modelname = PDB
chain = Chain
log.verbose()

env = environ(rand_seed=random.randint(-100000,100000))
env.io.hetatm = True
env.edat.dynamic_sphere=False
env.edat.dynamic_lennard=True
env.edat.contact_shell = 4.0
env.edat.update_dynamic = 0.39
env.libs.topology.read(file='$(LIB)/top_heav.lib')
env.libs.parameters.read(file='$(LIB)/par.lib')
mdl1 = model(env, file=modelname + '_' + Chain)
ali = alignment(env)
ali.append_model(mdl1, atom_files=modelname + '_' + Chain, align_codes=modelname+'_'+Chain)

for i in Var.split(','):
	respos, restyp = i[3:len(i)-3],i[len(i)-3:]
	s = selection(mdl1.chains[chain].residues[respos])
	s.mutate(residue_type=restyp)
	ali.append_model(mdl1, align_codes=modelname + '_' + Chain)

mdl1.clear_topology()
mdl1.generate_topology(ali[-1])
mdl1.transfer_xyz(ali)
mdl1.build(initialize_xyz=False, build_method='INTERNAL_COORDINATES')
mdl2 = model(env, file=modelname + '_' + Chain)
mdl1.res_num_from(mdl2,ali)
mdl1.write(file=modelname+ '_' + Chain + '.tmp')
mdl1.read(file=modelname + '_' + Chain + '.tmp')
make_restraints(mdl1, ali)
mdl1.env.edat.nonbonded_sel_atoms=1
sched = autosched.loop.make_for_model(mdl1)

for i in Var.split(','):
	respos, restyp = i[3:len(i)-3],i[len(i)-3:]
	s = selection(mdl1.chains[chain].residues[respos])
	mdl1.restraints.unpick_all()
	mdl1.restraints.pick(s)
	s.energy()
	s.randomize_xyz(deviation=4.0)
	mdl1.env.edat.nonbonded_sel_atoms=2
	optimize(s, sched)
	s.energy()
	make_restraints(mdl1, ali)
	mdl1.env.edat.nonbonded_sel_atoms=1
	sched = autosched.loop.make_for_model(mdl1)
mdl1.write(file=modelname + '_' + Chain + '_Mutated.pdb')
os.remove(modelname + '_' + Chain + '.tmp')







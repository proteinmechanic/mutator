mol load pdb [lindex $argv 0]
set id [lindex [split [lindex $argv 0] .] 0]

set p _Mutated

set pdb $id$p.pdb

set id1 [lindex [split $pdb .] 0]
set ch [lindex [split [lindex [split $pdb .] 0] _] 1]
set und _
set id2 psfgen

package require psfgen
topology top_all36_prot.rtf

pdbalias residue HIS HSE
pdbalias atom ILE CD1 CD

segment $ch {pdb $pdb}
coordpdb $pdb $ch
guesscoord

writepdb $id1$und$id2.pdb
writepsf $id1$und$id2.psf

